import java.util.Scanner;

/**
 * Created by timoha on 02.04.2016.
 */
public class HomeWork2 {

    public static void main (String[] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println("Input a:");
        int a=sc.nextInt();
        for (int i=1;i<=a;i++) {
            for (int j=1;(j<=i)&&(j<=a-i+1);j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
